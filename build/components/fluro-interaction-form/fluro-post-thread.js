app.directive('postThread', function() {
    return {
        restrict: 'E',
        transclude:true,
        scope:{
          definitionName:"=?type",
          host:"=?hostId",
          thread:"=?thread",
        },
        // template:'<div class="post-thread" ng-transclude></div>',
        link:function($scope, $element, $attrs, $ctrl, $transclude) {
            $transclude($scope, function (clone, $scope) {
                $element.replaceWith(clone); // <-- will transclude it's own scope
            });
        },
        controller:function($scope, $filter, $rootScope, FluroContent) {

        	$scope.outer = $scope.$parent;

            if(!$scope.thread) {
                $scope.thread = [];
            }

            //////////////////////////////////////////////////

            var hostID;
            var definitionName;
            
            //////////////////////////////////////////////////

            function reloadThread() {
                console.log('Reload thread', 'post.'+hostID+'.' + definitionName);
                return FluroContent.endpoint('post/' + hostID + '/' + definitionName, true, true)
                .query()
                .$promise
                .then(threadLoaded, threadError);
            }

            //////////////////////////////////////////////////

            function threadLoaded(res) {
                console.log('Thread reloaded', res);
                //All the posts
                var allPosts = res;

                //Break it up into nested threads
                $scope.thread = _.chain(res)
                .map(function(post) {
                    //Find all replies to this post
                    post.thread = _.filter(allPosts, function(sub) {
                        return (sub.reply == post._id);
                    });
                    //If it's a top level post then send it back
                    if(!post.reply) {
                        return post;
                    }
                })
                .compact()
                .value();
            }

            //////////////////////////////////////////////////

            function threadError(err) {
                console.log('Thread Error', err);
                $scope.thread = []
            }

            //////////////////////////////////////////////////
            //////////////////////////////////////////////////

            //Watch for when the host and the definition name is changed/set
        	$scope.$watch('host + definitionName', function() {

                hostID = $scope.host;
                definitionName = $scope.definitionName;

                if(!hostID || !definitionName) {
                    return;
                } 

                ////////////////////////////////////////////////////////////////////////

                //Get the thread refresh event
                var threadRefreshEvent = 'post.'+hostID+'.' + definitionName;

                console.log('LISTENING FOR', threadRefreshEvent);
                //When it's broadcast
                $rootScope.$on(threadRefreshEvent, reloadThread);

                ////////////////////////////////////////////////////////////////////////
                
                return reloadThread();

                ////////////////////////////////////////////////////////////////////////

                
        	})
        },
    }
});