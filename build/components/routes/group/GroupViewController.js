GroupRouteController.resolve = {
	item: function($stateParams, FluroContent) {
        return FluroContent.resource('get/' + $stateParams.id).get({}).$promise;
    },
    seo: function(FluroSEOService, Asset, item) {

        //Set the page title as the title of the article
        FluroSEOService.pageTitle = item.title;

        //Get variables from your article item
        var articleImage = _.get(item, 'data.publicData.image');
        var articleDescription = _.get(item, 'data.publicData.shortDescription');

        FluroSEOService.imageURL = Asset.imageUrl(articleImage, 640);
        FluroSEOService.description = articleDescription;
        return true;
    },
}

/////////////////////////////////////////////////////

function GroupRouteController($scope, $rootScope, item) {

	$scope.item = item;


    var additionalFields = _.get($rootScope, 'applicationData.searchFilters') || [];
    
    $scope.additionalFields = _.chain(additionalFields)
    .map(function(field) {

        var value = _.get(item, field.path);

        if(!value) {
            return;
        }

        if(_.isArray(value)) {

            value = _.chain(value)
            .compact()
            .map(function(v) {
                if(v.title) {
                    return v.title;
                }

                if(v.name) {
                    return v.name;
                }

                return v;
            })
            .uniq()
            .value()
            .join(', ');
        } else {
            if(value.title) {
                value = value.title;
            }

            if(value.name) {
                value = value.name;
            }
        }

        return {
            label:field.label,
            value:value,
        }
    })
    .compact()
    .value();

	
}

