HomeRouteController.resolve = {
	seo: function(FluroSEOService, Asset, $rootScope) {
        FluroSEOService.pageTitle = null;

        return true;
    },
    groups:function(FluroContent, $rootScope) {



        // If there is a custom query selected on the Application use it
        // var groupsQuery = _.get($rootScope.applicationData, 'groupsQuery');
        // console.log('Data:',groupsQuery,  $rootScope.applicationData)

        var groupsQueryID =  _.get($rootScope.applicationData, 'groupsQuery._id');

        if(groupsQueryID) {
            return FluroContent.endpoint('content/_query/' + groupsQueryID).query().$promise;
        } 

        // otherwise, use the generic query.
        return FluroContent.endpoint('content/_query/5a979ca6bfc3ba79037cd62c').query().$promise;
        

        
    }
}

function HomeRouteController($scope, $rootScope, groups, $filter) {


    $scope.search = {
        terms:''
    };


    var additionalFilters = _.get($rootScope, 'applicationData.searchFilters') || [];
    $scope.additionalFilters = additionalFilters;

    ///////////////////////////////////////////
    
    $scope.$watch('search', updateSearch, true);

    ///////////////////////////////////////////

    

    $scope.hasSearched = function() {
        var hasSearched = (_.keys($scope.search).length > 1) || ($scope.search.terms && $scope.search.terms.length);
        return hasSearched;
    }

    $scope.noResults = function() {

        var hasSearched = $scope.hasSearched();
        var noItems = !$scope.filteredItems || !$scope.filteredItems.length;
        return hasSearched && noItems;
    }

    ///////////////////////////////////////////


    $scope.getFilterOptions = function(filterSet) {

        var path = filterSet.path;

        /////////////////////////////////////////

        return _.chain(groups)
        .map(function(group) {
            return _.get(group, path);
        })
        .flatten()
        .uniq()
        .compact()
        .orderBy(function(v) {
            return v;
        })
        .value();


    }


    ///////////////////////////////////////////

    $scope.resetSearch = function() {
        $scope.search = {
            terms:'',
        };

        updateSearch();
    }

    ///////////////////////////////////////////

    $scope.searchSummary = function() {

        if(!$scope.hasSearched()) {
            return;
        }

        ///////////////////////////////////////////

        var terms = $scope.search.terms;
        var weekday = $scope.search.weekday;
        var age = $scope.search.age;
        var suburb = $scope.search.suburb;

        var pieces = _.compact([
            terms,
            weekday,
            age,
            suburb
        ])

        if($scope.filteredItems.length && pieces.length) {

            //Change the word 'match' to 'matches' depending on how many items are found
            if($scope.filteredItems.length == 1) {
                return $scope.filteredItems.length + ' match for \'' + (pieces).join(', ') + '\'';
            } else {
                return $scope.filteredItems.length + ' matches for \'' + (pieces).join(', ') + '\'';
            }


            
        } 

        return $scope.filteredItems.length + ' groups match your criteria';

        
    }
    ///////////////////////////////////////////
    ///////////////////////////////////////////

    function updateSearch() {

        var filteredItems = groups;
        var search = $scope.search;

        var terms = search.terms;
        var weekday = search.weekday;
        var age = search.age;
        var suburb = search.suburb;



        /////////////////////////////////////////////////

        
        //Filter by search terms
        if($scope.search.terms.length) {
            filteredItems = $filter('filter')(filteredItems, terms);
        }
        
        ///////////////////////////////////////////

        // Now apply filters
        if ($scope.search.weekday) {
            filteredItems = _.filter(filteredItems, function(item) {

                // console.log(item.data.weekday, weekday)
                if(item.data && item.data.weekday) {
                    return _.capitalize(item.data.weekday) == weekday;    
                }
                
            });
        }
        
        if ($scope.search.age) {
            filteredItems = _.filter(filteredItems, function(item) {

                // console.log(item.data, age)

                if(item.data && item.data.age) {
                    return _.includes(item.data.age, age);
                }
            });
        }
        
        if ($scope.search.suburb) {
            filteredItems = _.filter(filteredItems, function(item) {

                if(item.data && item.data.address) {
                    return item.data.address.suburb == suburb;
                }
                
            });
        }
        
        ///////////////////////////////////////////////

        // console.log('SEARCH ADDITIONAL', search);

        _.each(search.additional, function(v, key) {

            if(!v || !v.length) {
                return;
            }
        
            //Each additional filter
            filteredItems = _.filter(filteredItems, function(item) {

                var searchData = search[key];
                var itemData = _.get(item, key);


                if(_.isArray(itemData)) {
                    return _.includes(itemData, v);
                } else {
                    return itemData == v;
                }

                // if(item.data && item.data.address) {
                //     return item.data.address.suburb == suburb;
                // }
                
            });
            
        });

        //console.log(filteredItems)
        ///////////////////////////////////////////////


        $scope.weekdayOptions = _.chain(filteredItems)
        .map(function(item) {
            if(item.data) {
                return _.capitalize(item.data.weekday);
            }
        })
        .compact()
        .uniq(function(item) {
            return item;
        })
        .sortBy(function(item) {
            var dateOrder = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
            return dateOrder.indexOf(item);

        })
        .value();


        //this is the one for group 'Types'

        $scope.ageOptions = _.chain(filteredItems)
        .map(function(item) {
            // if(item.data && item.data.age) {
                return _.get(item, 'data.age');
            // } else {
                // return 'Any';
            // }
        })
        .flatten()
        .compact()
        .uniq(function(item) {
            return item;
        })
        .value();
        
        // console.log('dem age options man', $scope.ageOptions)

        $scope.suburbOptions = _.chain(filteredItems)
        .map(function(item) {
            return _.get(item, 'data.address.suburb');
        })
        .compact()
        .uniq(function(item) {
            return item;
        })
        .sortBy(function(v) {
            return v;
        })
        .value();





        $scope.filteredItems = _.orderBy(filteredItems, function(item) {
            return item.title;
        });
    }
}

