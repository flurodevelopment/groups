

DetailsRouteController.resolve = {
	item: function($stateParams, FluroContent) {
        return FluroContent.resource('get/' + $stateParams.id).get({}).$promise;
    },
    contactDetails: function($stateParams, FluroContent) {

    // https://api.fluro.io/content/_query/59e051be0ae81b0e7362772b?variables[GROUP_ID]=59dde3be5d06c3362e31477f&noCache=true&access_token=ua-$2a$10$utfK/at6ryJBO1GpbwCdauozbt00SWOSs5AvIxKkahvsXO5B8AjH.
        return FluroContent.resource('_query/59e051be0ae81b0e7362772b?variables[GROUP_ID]=' + $stateParams.id).query({
            // variables:{
            //     GROUP_ID:"59dde3be5d06c3362e31477f"
            // }
        }).$promise;
    },





    seo: function(FluroSEOService, Asset, item) {

        //Set the page title as the title of the article
        FluroSEOService.pageTitle = item.title;

        //Get variables from your article item
        var articleImage = _.get(item, 'data.publicData.image');
        var articleDescription = _.get(item, 'data.publicData.shortDescription');

        FluroSEOService.imageURL = Asset.imageUrl(articleImage, 640);
        FluroSEOService.description = articleDescription;
        return true;
    },
}

/////////////////////////////////////////////////////

// app.controller('JoinController', function($scope) {

// 	console.log('JOIN CONTROLLEr')
	
// });

/////////////////////////////////////////////////////

function DetailsRouteController($scope, item, contactDetails) {

	$scope.item = item;

    if(contactDetails) {
        $scope.leaders = _.chain(contactDetails[0].assignments)
        .map(function(row) {
            return row.contacts;
        })
        .flatten()
        .value();
    }
    


}

