app.directive('joinGroupForm', function(FluroContent, $rootScope) {
    return {
        restrict: 'E', 
        replace:true, 
        scope:{
            group:'=ngModel',
        },
        templateUrl: 'join-group-form/join-group-form.html', 
        link: function($scope, $elem, $attr) {
            
            $scope.contact = {};

            if($rootScope.applicationData.formOptions.hideGender = true) {
                $scope.contact.gender = "unknown"
            }

            /////////////
            
            var hasTerms = _.get($rootScope, 'applicationData.terms');

            $scope.isValid = function() {
                
                if(!$scope.contact.firstName || !$scope.contact.firstName.length) {
                    return;
                }
                
                if(!$scope.contact.lastName || !$scope.contact.lastName.length) {
                    return;
                }
                
                if(!$scope.contact.email) {
                    return;
                }
                
                if(!$scope.contact.phoneNumber) {
                    return;
                }

                if(!$scope.contact.gender && !$rootScope.applicationData.formOptions.hideGender) {
                    return;
                }

                if(hasTerms && hasTerms.length && !$scope.contact.agreedToTerms) {
                    return;
                }
               
                return true;
            }   
            
    
            
            
            $scope.submit = function() {
                
                $scope.contact.processing = true;
                
                //do your thing
                var request = FluroContent.endpoint('teams/' + $scope.group._id + '/join').save($scope.contact).$promise;
            
                request.then(success, failed);
                
                function success(res) {
                    console.log('Joined group successfully', res);
                    $scope.contact.processing = false;
                    $scope.contact.joined = true;
                }
                
                function failed(err) {
                    console.log('Error joining group!', err)
                    $scope.contact.processing = false;
                    
                }
            }
            
        } 
    } 
});