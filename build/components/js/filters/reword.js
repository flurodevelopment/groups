app.filter('reword', function($rootScope) {

    console.log('Get Translations')
    var translations = _.get($rootScope.applicationData, 'translations');

    return function(string) {

        //Loop through each translatedWord
        _.each(translations, function(set) {

            // console.log('REPLACE', set.from, string);
            string = string.replace(new RegExp(set.from, 'g'), set.to);
        });

        // console.log('Replaced', string);
        return string


    };

});