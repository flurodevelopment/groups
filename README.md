# fluro angular boilerplate
Starter kit for building a fluro angular app

# Getting Started
~~~~
git clone https://bitbucket.org/flurodevelopment/groups flurogroups
~~~~

Once you have cloned the repository change into the directory where you cloned to and install node packages
~~~~
cd flurogroups
npm install
~~~~

And change into the '/app' directory and install all bower components
~~~~
cd app
bower install
cd ../
~~~~

# Starting Grunt and developing
Once bower and node packages have been installed start grunt using
~~~~
grunt
~~~~

This will start watching changes in style.scss file, all .js, .html and .scss files located anywhere in the 'build/components' folder, Any changes will automatically trigger SCSS to compile, Javascript to concatenate and HTML to be made into angular templates, this will also start the livereload service in your browser window.
You will be able to run your application and view your changes in realtime in the browser by visiting http://0.0.0.0:9001 or http://localhost:9001


# Packaging for final distribution
~~~~
grunt build
~~~~

This will concatenate, compile, compress, minify and copy your publishable application into the 'dist' directory ready to be deployed


# Deploying to Fluro
You can now commit and push to your remote git repository. 
Then create or redeploy an existing deployment at https://admin.fluro.io/deployment


Happy coding!